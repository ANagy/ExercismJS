//
// This is only a SKELETON file for the 'Protein Translation' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

/**
 * Enum for protein names and a STOP value
 * @readonly
 * @enum {string}
 */
const proteinNameEnum = Object.freeze({
  Methionine:    `Methionine`,
	Phenylalanine: `Phenylalanine`,
	Leucine:       `Leucine`,
	Serine:        `Serine`,
	Tyrosine:      `Tyrosine`,
	Cysteine:      `Cysteine`,
	Tryptophan:    `Tryptophan`,
	STOP:          '' // To stop transcription
});

/**
 *  Enum for mapping three-letter codons to protein names
 * @readonly
 * @enum {string}
 */
const codonToProteinEnum = Object.freeze({
  AUG: proteinNameEnum.Methionine,
  UAA: proteinNameEnum.STOP,
  UAC: proteinNameEnum.Tyrosine,
  UAG: proteinNameEnum.STOP,
  UAU: proteinNameEnum.Tyrosine,
  UCA: proteinNameEnum.Serine,
  UCC: proteinNameEnum.Serine,
  UCG: proteinNameEnum.Serine,
  UCU: proteinNameEnum.Serine,
  UGA: proteinNameEnum.STOP,
  UGC: proteinNameEnum.Cysteine,
  UGG: proteinNameEnum.Tryptophan,
  UGU: proteinNameEnum.Cysteine,
  UUA: proteinNameEnum.Leucine,
  UUC: proteinNameEnum.Phenylalanine,
  UUG: proteinNameEnum.Leucine,
  UUU: proteinNameEnum.Phenylalanine,
});

/**
 * Returns a array of valid proteins for a passed RNA sequence
 * @param {string} rnaSequence RNA sequence string
 * @returns {[string]} Array of protein names
 */
export const translate = (rnaSequence) => {
  if (typeof rnaSequence != `string`) return [];

  const regexMatches = rnaSequence.match(/(.{1,3})/g);
  const translatedProteins = [];

  regexMatches.every(match => {
    const protein = codonToProteinEnum[match];

    if (protein === undefined) throw new Error(`Invalid codon`);
    else if (protein === proteinNameEnum.STOP) return false;
    else translatedProteins.push(protein);
    
    return true;
  });

  return translatedProteins;
};
