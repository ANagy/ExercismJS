//
// This is only a SKELETON file for the 'Roman Numerals' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

/**
 * Converts a passed number to its Roman numeral notation string.
 * @param {number} numberToConvert An integer number between 1 and 3999
 * @returns {string} Output in Roman numeral notation
 */
export const toRoman = (numberToConvert) => {
  if (numberToConvert < 1
    || numberToConvert > 3999) return ``; 

  const lookupForNumberToRomanNotation = new Map([
    [1000, `M`],
    [900, `CM`], // special case
    [500, `D`],
    [400, `CD`], // special case
    [100, `C`],
    [90, `XC`], // special case
    [50, `L`],
    [40, `XL`], // special case
    [10, `X`],
    [9, `IX`], // special case
    [5, `V`],
    [4, `IV`], // special case
    [1, `I`],
    ]);

  let convertedRomanNotation = ``;

  for (const [partialMatch, romanTranslation] of
    lookupForNumberToRomanNotation) {
    while (numberToConvert >= partialMatch) {
      convertedRomanNotation += romanTranslation;
      numberToConvert -= partialMatch;
    }
  }

  return convertedRomanNotation;
};
