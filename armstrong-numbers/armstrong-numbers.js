//
// This is only a SKELETON file for the 'Armstrong Numbers' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

/**
* Tests whether a number is an Armstrong number 
* (i.e. the sum of its own digits each raised to the power of the number of digits).
* @param {number} numberToCheck to test
* @returns {boolean} Whether the number is an Armstrong numbeer
**/
export const isArmstrongNumber = numberToCheck => {

  let tally = 0;
  const digits = numberToCheck.toString();
  for (const digit of digits) {
    tally += Math.pow(parseInt(digit), digits.length);
  }

  return numberToCheck === tally;
};
