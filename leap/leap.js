//
// This is only a SKELETON file for the 'Leap' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

/**
 * Tests where the passed year is a leap year in the Gregorian calendar
 * @param {number} year The year to test
 * @returns {boolean} Whether the passed year is a leap year 
*/
export const isLeap = year => 
  year % 4 === 0 && (year % 100 != 0 || year % 400 === 0)